import { ADD_ITEM, REMOVE_ITEM, INCREMENT_ITEM, DECREMENT_ITEM, INITIAL_CART } from './actions'
const initalState = {
    cart: [],
}

export default function cart(state = initalState, action) {
    switch (action.type) {

        case INITIAL_CART:
            {
                return {
                    ...state,
                    cart: []
                }
            }
        case ADD_ITEM:
            {
                let product = { ...action.payload, quantity: 1 }
                let itemExist = state.cart.find((item) => {
                    return item.id === product.id;
                })
                if (!itemExist) {
                    return {
                        ...state,
                        cart: [...state.cart, product]
                    }
                }
                else {
                    return state;
                }
            }
        case INCREMENT_ITEM:
            {
                let product = action.payload;
                return {
                    ...state,
                    cart: state.cart.map((item) => {
                        if (item.id === product.id) {
                            return {
                                ...item,
                                quantity:item.quantity + 1
                            };
                        }
                        else {
                            return item;
                        }
                    })
                }
            }
        case DECREMENT_ITEM:
            {
                let product = action.payload;
                return {
                    ...state,
                    cart: state.cart.map((item) => {
                        if (item.id === product.id && item.quantity > 1) {
                            return  {
                                ...item,
                                quantity:item.quantity - 1
                            };
                        }
                        else {
                            return item;
                        }
                    })
                }
            }
        case REMOVE_ITEM: {
            let product = action.payload;
            return {
                ...state,
                cart: state.cart.filter((item) => {
                    return item.id !== product.id;
                })
            }
        }
        default: {
            return initalState;
        }
    }
}