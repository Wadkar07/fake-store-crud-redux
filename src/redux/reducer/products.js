import { REMOVE_PRODUCT, INITIAL, EDIT_PRODUCT, ADD_PRODUCT } from './actions'
const initalState = {
    productsList: [],
}
export default function product(state = initalState, action) {
    switch (action.type) {

        case INITIAL:
            {
                return {
                    ...state,
                    productsList: action.payload
                }
            }

        case EDIT_PRODUCT:
            const product = action.payload;
            return {
                ...state,
                productsList: state.productsList.map((item) => {
                    if (item.id === product.id) {
                        return product;
                    } else {
                        return item;
                    }
                })
            }

        case REMOVE_PRODUCT:
            const removeProduct = action.payload;
            return {
                ...state,
                productsList: state.productsList.filter((item) => {
                    return item !== removeProduct;
                })
            }

        case ADD_PRODUCT:
            const addProductToList = action.payload;
            return {
                ...state,
                productsList: [...state.productsList, addProductToList]
            }

        default: {
            return state;
        }
    }
}