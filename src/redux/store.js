import {configureStore } from '@reduxjs/toolkit'
import products from './reducer/products'
import cart from './reducer/cart'


export default configureStore({
    reducer: {
        products,
        cart
    }
})