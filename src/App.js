import axios from 'axios';
import React, { Component } from 'react'
import Products from './components/Products';
import PreLoader from './components/PreLoader';
import Error from './components/Error';
import { connect } from 'react-redux';
import Navbar from './components/Navbar'
import Cart from './components/Cart';
import { Route, Routes } from 'react-router-dom';

class App extends Component {
  constructor(props) {

    super(props);

    this.STATE_API = {
      LOADING: 'loading',
      LOADED: 'loaded',
      ERROR: 'error'
    }

    this.state = {
      status: this.STATE_API.LOADING
    }
    this.URL = 'https://fakestoreapi.com/products';
  }

  fetchData(url) {
    axios
      .get(url)
      .then((response) => {
        this.setState({
          status: this.STATE_API.LOADED
        });
        this.props.initialProducts(response.data);
      })
      .catch((err) => {
        this.setState({
          status: this.STATE_API.ERROR
        });
      })
  }

  componentDidMount() {
    this.fetchData(this.URL)
  }

  render() {

    return (
      <div>
        <Navbar />
        {this.state.status === this.STATE_API.LOADING ? <PreLoader /> :
          this.state.status === this.STATE_API.ERROR ? <Error /> :
            <>
              <Routes>
                {this.props.products.length ? <Route path="/" element={<Products />} /> : <>No Product Found</>}
                <Route path="/cart" element={<Cart />} />
              </Routes>

            </>
        }
      </div>
    )
  }
}

const mapStateToProps = (state) => {
  const products = state.products.productsList;
  return {
    products
  }
}

const mapDispatchToProps = (dispatch) => {
  return {
    initialProducts: (products) =>
      dispatch({
        type: 'INITIAL',
        payload: products
      }),
  };
}


export default connect(mapStateToProps, mapDispatchToProps)(App)