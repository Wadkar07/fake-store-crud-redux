import React, { Component } from 'react'
import Product from './Product'
import { connect } from 'react-redux'
import AddProduct from './AddProduct'

class Products extends Component {
  render() {
    return (
      <div>
        <AddProduct />

        {
          this.props.products.map((product) => {
            return (
              <Product product={product} key={product.id} />
            )
          })
        }
      </div>
    )
  }
}

function mapStateToProps(state) {
  return {
    products: state.products.productsList,
  }
}

export default connect(mapStateToProps)(Products);