import React from 'react'
import './Cart.css'
import { connect } from 'react-redux'


class CartItem extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            showCartItem: true,
            remove: false
        }
    }

    handleRemove = () => {
        this.props.removeItem(this.props.item)
        this.setState({
            showCartItem: false,
        })
    }
    render() {
        return (
            <>
                {
                    this.state.showCartItem &&
                    (<div>
                        <div className='cartItem'>
                            <img className='image' src={this.props.item.image} alt={this.props.item.title} />
                            <h2 className='title'>{this.props.item.title}</h2>
                            <h2 className='price'>{(this.props.item.price * this.props.item.quantity).toFixed(2)}$</h2>
                            <div className='quantity-div'>
                                <button onClick={() => {
                                    this.props.incrementItem(this.props.item)
                                }}>+</button>
                                <h4 className='quantity'>{this.props.item.quantity}</h4>
                                <button onClick={() => {
                                    this.props.decrementItem(this.props.item)
                                }}>-</button>
                            </div>
                            {!this.state.remove ?
                                <button className='remove-item' onClick={() => {
                                    this.setState({
                                        remove: true
                                    })
                                }}>Remove Item</button>
                                : <div className='delete-prompt remove-item cart-delete'>
                                    want to delete
                                    <div className="delete-btn">
                                        <button className='remove-item' onClick={this.handleRemove}>Yes</button>
                                        <button className='remove-item' onClick={()=>{
                                            this.setState({
                                                remove: false
                                            })
                                        }}>No</button>
                                    </div>
                                </div>}
                        </div>
                    </div>)
                }
            </>
        )
    }
}


const mapDispatchToProps = (dispatch) => {
    return {
        removeItem: (cartItems) => {
            dispatch({
                type: 'REMOVE_ITEM',
                payload: cartItems
            })
        },
        incrementItem: (cartItems) => {
            dispatch({
                type: 'INCREMENT_ITEM',
                payload: cartItems
            })
        },
        decrementItem: (cartItems) => {
            dispatch({
                type: 'DECREMENT_ITEM',
                payload: cartItems
            })
        }
    };
}



export default connect(null, mapDispatchToProps)(CartItem)