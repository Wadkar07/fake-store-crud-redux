import React, { Component } from 'react'
import './Navbar.css'
import logo from '../images/logo.png'
import { Link } from 'react-router-dom'

export default class Navbar extends Component {
    render() {
        return (
            <div className='navbar'>
                <Link to='/'>
                <img src={logo} alt="" className="logo-image" />
                </Link>
                <Link to='/cart'>
                <i className="fa-solid fa-cart-shopping cart-icon"></i>
                </Link>
            </div>
        )
    }
}
