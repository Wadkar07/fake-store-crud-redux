import React from 'react';
import { connect } from 'react-redux';
import CartItem from './CartItem';


class Cart extends React.Component {
  render() {
    return (
      this.props.cartItems.length ?
        <>
          {this.props.cartItems.map((item) => {
            return (
              <CartItem key={item.id} item={item} />
            )
          })}
        </> :
        <div className='no-product'>
          <h1>No Product in cart</h1>
        </div>
    )
  }
}

function mapStateToProps(state) {
  return {
    cartItems: state.cart.cart,
  }
}

function mapDispatchToProps(dispatch) {
  return {
    initialProducts: (cartItems) => {
      dispatch({
        type: 'INITIAL_CART',
        payload: cartItems
      })
    },
    removeItem: (cartItems) => {
      dispatch({
        type: 'REMOVE_ITEM',
        payload: cartItems
      })
    }
  };
}



export default connect(mapStateToProps, mapDispatchToProps)(Cart)